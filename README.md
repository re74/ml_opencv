#  The ML recognition of handwritten letters

 The project is aimed to get acquainted with machine learning and openCV library.
 The experiment shows 89 % accuracy using SVM, and 87 % using kNN, 93 % accuracy using CNN.

## Usage

* Clone the repository.
* Install dependacy (see instructions [here (openCV)](https://opencv.org/), [here(Scikit-learn)](https://scikit-learn.org/stable/), [here(Tensorflow)](https://www.tensorflow.org/), and [here (Matplotlib)](https://matplotlib.org/))
* Open the ***main.ipynb*** notebook  and run the cell.
* First, you see the input image. Press **q** to dismiss.
* Then you'll see the result under the cell: 

```sh
svm accuracy:  89.0
kNN opencv accuracy:  87.0
kNN sklearn accuracy:  87.0
CNN accuracy:  0.9300000071525574

```

## Dependency

* [OpenCV](https://opencv.org/)
* [Scikit-learn](https://scikit-learn.org/stable/)
* [Tensorflow](https://www.tensorflow.org/)
* [Matplotlib](https://matplotlib.org/)

## Maintainers

@MariaNema

